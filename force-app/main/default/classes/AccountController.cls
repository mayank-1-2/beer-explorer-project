public with sharing class AccountController {
    @AuraEnabled(cacheable=true)
    public static List<account>  getAccountList(){
        return [SELECT Id,Name,Phone,NumberOfEmployees,Type,AnnualRevenue FROM Account order by createddate desc];
    }
}
