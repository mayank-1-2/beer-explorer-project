/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-24-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-24-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/

import { LightningElement, api, wire, track } from 'lwc';
import { fire, register } from 'c/pubsub';

export default class RecordForm extends LightningElement {

    @track message = '';
    @track showcreateform = true;

    connectedCallback() {
        this.register();

    }

    register() {
        register("Details", this.handleEvent.bind(this));
    }

    handleEvent(messageFromEvt) {
        window.console.log("event handled ", messageFromEvt);
        this.message = messageFromEvt
            ? messageFromEvt.Id
            : "no message payload";
        this.showcreateform = false;

    }

    handleReset() {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }
    handleback() {
        this.showcreateform = true;
    }

    handleSuccess() {
        this.handleReset();

        let message = {
            "message": 'Success'
        }
        fire('success', message);
    }
    changelayout() {
        this.handleSuccess();
        this.showcreateform = true;
    }

}
