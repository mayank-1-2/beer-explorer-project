import { LightningElement, api, track, wire } from 'lwc';
import getLatestAccounts from '@salesforce/apex/AccountController.getAccountList';
import { refreshApex } from '@salesforce/apex';

import pubsub from 'c/pubsub';

export default class RecordShow extends LightningElement {
    message;
    @track accountList = [];
    @track error;
    @track wiredAccountList = [];
    @track pageNumber = 1;
    @track recordsPerPage = 10;
    @track totalPages;
    @track recordsToDisplay = [];
    @track showNext;
    @track showPrev;
    @track receivedMessage;
    @track totalrecord = 10;
    @track startingrecord = 1;
    @track pages = [];
    @track chunks = 1;
    @track pagestodisplay = [];
    @track isdisablednext = false;
    @track isdisabledprev = true;
    @track link;

    get options() {
        return [
            { label: '10', value: 10 },
            { label: '15', value: 15 },
            { label: '20', value: 20 },
        ];
    }

    getlink(e){
        // this.link = "/"+(e.target.dataset.id);
        console.log("grteywqs");
        console.log((e.target.dataset.id));
        
    }

    connectedCallback() {
        this.register();

    }

    register() {
        pubsub.register("success", this.handleEvent.bind(this));
    }

    renderedCallback() {
        this.renderButtons();
    }

    renderButtons = () => {
        this.template.querySelectorAll('.pagination').forEach((but) => {
            //console.log(this.pageNumber,parseInt(but.dataset.id,10));
            but.style.backgroundColor = this.pageNumber === parseInt(but.dataset.id, 10) ? 'blue' : 'white';
            but.style.color = this.pageNumber === parseInt(but.dataset.id, 10) ? 'white' : 'blue';
            but.style.borderRadius = '20px';
            but.style.cursor = 'pointer';
        });
    }

    @wire(getLatestAccounts) accList(result) {
        this.wiredAccountList = result;

        if (result.data) {
            this.accountList = result.data;
            let numberOfPages = Math.ceil(this.accountList.length / this.recordsPerPage);
            for (let index = 1; index <= numberOfPages; index++) {
                this.pages.push(index);
            }
            this.pagestodisplay = this.pages.slice(0, 3);
            this.setRecordsToDisplay();
            this.error = undefined;
        } else if (result.error) {
            this.error = result.error;
            this.accountList = [];
        }
    }

   

    handleEvent(messageFromEvt) {
        window.console.log("event handled ", messageFromEvt);
            this.message = messageFromEvt
                ? messageFromEvt.message
                : "no message payload";
        if (this.message === 'Success') {
            this.rerender();
            this.setRecordsToDisplay();

        }
    }

    rerender() {
        return refreshApex(this.wiredAccountList);
    }
    increasechunk() {
        this.chunks += 1;
        console.log(this.chunks, Math.floor(this.pages.length / 3));
        if (this.chunks >= Math.floor(this.pages.length / 3)) {
            console.log('inside');
            this.pagestodisplay = this.pages.slice((this.chunks - 1) * 3, (this.chunks) * 3);
            this.isdisablednext = true;
        }
        else {
            this.pagestodisplay = this.pages.slice((this.chunks - 1) * 3, (this.chunks) * 3);
            this.isdisablednext = false;
        }
        // this.pageNumber = (this.chunks - 1) * 3;
        this.setRecordsToDisplay();


    }

    decreasechunk() {
        if (this.chunks === 1) {
            this.isdisabledprev = true;
            this.isdisablednext = false;
        }
        else {
            this.chunks -= 1;
            if (this.chunks === 1) {
                this.isdisabledprev = true;
                this.isdisablednext = false;
            }
            this.pagestodisplay = this.pages.slice((this.chunks - 1) * 3, (this.chunks) * 3);

        }
    }

    handleChange(event) {
        this.chunks = 1;
        this.isdisablednext = false;
        this.pageNumber = 1;
        this.recordsPerPage = event.target.value;
        this.pages = [];
        let numberOfPages = Math.ceil(this.accountList.length / this.recordsPerPage);
        for (let index = 1; index <= numberOfPages; index++) {
            this.pages.push(index);
        }
        this.pagestodisplay = [];
        this.pagestodisplay = this.pages.slice(0, 3);
        //console.log(this.recordsPerPage);
        this.setRecordsToDisplay();
    }


    onPageClick = (e) => {
        this.pageNumber = parseInt(e.target.dataset.id, 10);
        this.setRecordsToDisplay();
    }
    previousPage() {

        this.pageNumber = this.pageNumber - 1;
        if(this.pageNumber <=this.chunks*3){
            this.decreasechunk();
        }
        this.setRecordsToDisplay();
    }
    nextPage() {
        this.pageNumber = this.pageNumber + 1;
        if (this.pageNumber >this.chunks*3){
            this.increasechunk();
        }
        else{
        this.setRecordsToDisplay();}
    }

    setRecordsToDisplay() {
        console.log(Math.ceil(this.pages.length / 3));
        if (this.chunks >= Math.ceil(this.pages.length / 3)) {
            this.isdisablednext = true;
        }
        if (this.chunks === 1) {
            this.isdisabledprev = true;
            if (this.chunks >= Math.ceil(this.pages.length / 3)) {
                this.isdisablednext = true;
            }
        } else {
            this.isdisabledprev = false;
        }
        //console.log('Inside');
        this.recordsToDisplay = [];
        // this.totalPages = Math.ceil(this.accountList.size()/this.recordsPerPage);
        this.totalrecord = this.pageNumber * this.recordsPerPage;
        this.startingrecord = ((this.pageNumber - 1) * this.recordsPerPage) + 1;
        //console.log('Inside2');
        // this.setPaginationControls();
        //console.log((this.pageNumber)*this.recordsPerPage);
        //console.log(this.accountList.length)
        if ((this.pageNumber * this.recordsPerPage) > this.accountList.length - (this.accountList.length) % this.recordsPerPage) {
            this.showNext = false;
            this.totalrecord = this.accountList.length;
        }
        else {
            this.showNext = true;
        }
        if ((this.pageNumber === 1) || (this.pageNumber * this.recordsPerPage) < 0) {
            this.showPrev = false;
        }
        else {
            this.showPrev = true;
        }

        //console.log("pagenumber ==>",this.pageNumber);
        //console.log("recordpagenumber ==>",this.recordsPerPage);
        if ((this.pageNumber * this.recordsPerPage) > this.accountList.length - (this.accountList.length) % this.recordsPerPage) {
            console.log((this.pageNumber * this.recordsPerPage), this.accountList.length - (this.accountList.length) % this.recordsPerPage, ((this.pageNumber - 1) * this.recordsPerPage) + ((this.accountList.length) % this.recordsPerPage));
            for (let i = (this.pageNumber - 1) * this.recordsPerPage; i < ((this.pageNumber - 1) * this.recordsPerPage) + ((this.accountList.length) % this.recordsPerPage); i++) {
                let tempRecord = Object.assign({}, this.accountList[i]); //cloning object  
                tempRecord.recordLink = "/" + tempRecord.Id;
                this.recordsToDisplay.push(tempRecord);
            }
        }
        else {
            for (let i = (this.pageNumber - 1) * this.recordsPerPage; i < this.pageNumber * this.recordsPerPage; i++) {
                let tempRecord = Object.assign({}, this.accountList[i]); //cloning object  
                tempRecord.recordLink = "/" + tempRecord.Id;
                this.recordsToDisplay.push(tempRecord);
            }

        }
        //console.log(this.recordsToDisplay);
    }

    sendDetails(e){
        console.log(e.target.dataset.id);
        let message={
            "Id" : e.target.dataset.id
        }
        pubsub.fire('Details',message);
    }




}